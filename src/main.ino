#include "Arduboy.h"
#include "HID-Project.h"
#include "SingleNSGamepad.h"

#define LINE_WIDTH 20
#define LINE_HEIGHT 8

#define BLINKDELAY 500
#define UI_DEBOUNCE_MS 300

#define MACRO_PAUSED -1

/*
NS Button   |Button Name    |ItsyBitsy Pin
------------|---------------|-------------
0           |Y              |0
1           |B              |1
2           |A              |2
3           |X              |3
4           |L              |4
5           |R              |7
6           |ZL             |9
7           |ZR             |10
8           |MINUS          |11
9           |PLUS           |12
10          |LSB#           |13
11          |RSB#           |SCK
12          |HOME           |A4
13          |CAPTURE        |A5

# LSB/RSB = thumbsticks are also buttons
*/

const uint8_t DPAD_MAP[16] = {
    NSGAMEPAD_DPAD_CENTERED,   // 0000 All dpad buttons up
    NSGAMEPAD_DPAD_UP,         // 0001 direction UP
    NSGAMEPAD_DPAD_RIGHT,      // 0010 direction RIGHT
    NSGAMEPAD_DPAD_UP_RIGHT,   // 0011 direction UP RIGHT
    NSGAMEPAD_DPAD_DOWN,       // 0100 direction DOWN
    NSGAMEPAD_DPAD_CENTERED,   // 0101 invalid
    NSGAMEPAD_DPAD_DOWN_RIGHT, // 0110 direction DOWN RIGHT
    NSGAMEPAD_DPAD_CENTERED,   // 0111 invalid
    NSGAMEPAD_DPAD_LEFT,       // 1000 direction LEFT
    NSGAMEPAD_DPAD_UP_LEFT,    // 1001 direction UP LEFT
    NSGAMEPAD_DPAD_CENTERED,   // 1010 invalid
    NSGAMEPAD_DPAD_CENTERED,   // 1011 invalid
    NSGAMEPAD_DPAD_DOWN_LEFT,  // 1100 direction DOWN LEFT
    NSGAMEPAD_DPAD_CENTERED,   // 1101 invalid
    NSGAMEPAD_DPAD_CENTERED,   // 1110 invalid
    NSGAMEPAD_DPAD_CENTERED,   // 1111 invalid
};

uint8_t dpad_bits = 0;
String controller_setup[] = {"P4",    "P5", "W200", "R4", "R5",
                             "W1000", "P2", "W300", "R2"};
String autocook[] = {
"P9", "W300", "R9", // +
"P2", "W300", "R2", // A
"PD", "W100", "RD", // down down
"W300", // wait
"PD", "W100", "RD", // down down
"W1000", // wait
"P2", "W300", "R2", // A
"W1000", // wait
"P2", "W300", "R2", // A
"W750", // wait
"P1", "W300", "R1", // B
"W1000", // wait
"P2", "W300", "R2", // A
"W300", // wait
"P3", "W300", "R3", // X
"W1000", // wait
"P1", "W300", "R1", // B
"W300", // wait
"P1", "W300", "R1", // B
};


int macro_index = MACRO_PAUSED;
String *current_macro = controller_setup;

Arduboy arduboy;
unsigned long nextInteractionMs = 0;

void setup() {
  arduboy.beginNoLogo();
  arduboy.setFrameRate(15);
  arduboy.clear();
  arduboy.setCursor(0, 0);
  arduboy.setTextWrap(false);
  arduboy.setRGBled(0, 0, 0);
  arduboy.println("Hello, world!");
  arduboy.display();
  NSGamepad.begin();
}

size_t macro_size = 0;

void loop() {
  unsigned long currentMillis = millis();

  static uint8_t dpad1 = GAMEPAD_DPAD_CENTERED;
  if (arduboy.pressed(A_BUTTON)) {
    current_macro = controller_setup;
    macro_index = 0;
    macro_size = 9;
    arduboy.clear();
    arduboy.setCursor(0, 0);
  }
  if (arduboy.pressed(B_BUTTON)) {
    current_macro = autocook;
    macro_index = 0;
    macro_size = 41;
    arduboy.clear();
    arduboy.setCursor(0, 0);
  }
  if (arduboy.pressed(UP_BUTTON)) {
  }
  if (arduboy.pressed(RIGHT_BUTTON)) {
  }
  if (arduboy.pressed(DOWN_BUTTON)) {
  }
  if (arduboy.pressed(LEFT_BUTTON)) {
  }

  if (currentMillis > nextInteractionMs) {
    nextInteractionMs = currentMillis;

    if (macro_index == MACRO_PAUSED) {
      arduboy.clear();
      arduboy.setCursor(0, 0);
      arduboy.println("Press A or B to start macro");
      nextInteractionMs = currentMillis + UI_DEBOUNCE_MS;
    } else {
      String command = current_macro[macro_index++];
      if (command == NULL) {
        macro_index = MACRO_PAUSED;
        nextInteractionMs = currentMillis + UI_DEBOUNCE_MS;
        return;
      }
      if (macro_index % LINE_HEIGHT == 0) {
        arduboy.clear();
        arduboy.setCursor(0, 0);
      }
      arduboy.print("Step ");
      arduboy.print(macro_index);
      arduboy.print(": ");
      arduboy.println(command);
      if (command[0] == 'P') {
        if (command[1] == 'D') {
          NSGamepad.dPad(NSGAMEPAD_DPAD_DOWN);
        } else {
          int button = command.substring(1).toInt();
          NSGamepad.press(button);
        }
      } else if (command[0] == 'R') {
        if (command[1] == 'D') {
          NSGamepad.dPad(NSGAMEPAD_DPAD_CENTERED);
        } else {
          int button = command.substring(1).toInt();
          NSGamepad.release(button);
        }
      } else if (command[0] == 'W') {
        int delay = command.substring(1).toInt();
        nextInteractionMs = currentMillis + delay;
      }
    }

    if (macro_index >= macro_size) {
      macro_index = MACRO_PAUSED;
      nextInteractionMs = currentMillis + UI_DEBOUNCE_MS;
    }
  }

  NSGamepad.loop();
  arduboy.display();
}
